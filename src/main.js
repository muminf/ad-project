// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import * as fb from 'firebase'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    var config = {
      apiKey: 'AIzaSyCDdZf-8CSl1QC9IGAR9xjRmswEynItyUk',
      authDomain: 'vue-ad-project-9d451.firebaseapp.com',
      databaseURL: 'https://vue-ad-project-9d451.firebaseio.com',
      projectId: 'vue-ad-project-9d451',
      storageBucket: 'vue-ad-project-9d451.appspot.com',
      messagingSenderId: '1090197923058'
    }
    fb.initializeApp(config)
    fb.auth().onAuthStateChanged(user => {
      if (user) {
        this.$store.dispatch('autoLoginUser', user)
      }
    })
    this.$store.dispatch('fetchAds')
  }

})
